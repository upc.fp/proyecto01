import java.util.Scanner;

public class Ejercicio01
{
    public static void main(String[] args)
    {
        //01. Declaracion de Variables
        Scanner sc = new Scanner(System.in);
        int gaseosas;
        int panes;

        double monto;
        double monto_sanguches;
        double dscto;
        double monto_total;

        //02. Captura de Valores
        System.out.println("Ingrese el número de bebidas consumidas: ");
        gaseosas = sc.nextInt();

        System.out.println("Ingrese el número de sanguches consumidos: ");
        panes = sc.nextInt();

        //03. Cálculos
        monto = gaseosas * 5.00;
        monto_sanguches = panes * 4.00;
        dscto = (monto * 0.1) +  (monto_sanguches * 0.1);
        monto_total = monto + monto_sanguches - dscto;

        //04. Resultado
        System.out.printf("\nEl importe a sobrar es: %.2f \n", monto_total);
    }
}
